using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendsProblem
{
    class FriendsMethod
    {
        public static void AcceptUserInputs()
        {
            Stack<int> friendsStack = new Stack<int>(10000);
            int noOfFriends = 0, removeFriends = 0;
            int noOftestCase;
            noOftestCase = int.Parse(Console.ReadLine());
            if (noOftestCase >= 1 && noOftestCase <= 1000)
            {
                for (int testCase = 0; testCase < noOftestCase; testCase++)
                {
                    friendsStack.Clear();
                    var values = Console.ReadLine().Split(' ');
                    noOfFriends = int.Parse(values[0]);
                    removeFriends = int.Parse(values[1]);
                    if(noOfFriends>=1 && noOfFriends<=100000 && removeFriends>=1 && removeFriends<noOfFriends)
                    {
                        string[] friendsPopularity = Console.ReadLine().Split(' ');
                        DeleteFriends(friendsStack, noOfFriends, removeFriends, friendsPopularity);
                        FinalFriendList(friendsStack);
                    }
                }
            }
        }

        public static void DeleteFriends(Stack<int> friendsStack, int noOfFriends, int removeFriends, string[] friendsPopularity)
        {
            int popCount, index;
            for (popCount = 0, index = 0; index < noOfFriends; index++)
            {
                int popularity = int.Parse(friendsPopularity[index]);
                if (popularity >= 0 && popularity <= 100)
                {
                    while (popCount < removeFriends && friendsStack.Count > 0 && friendsStack.Peek() < popularity)
                    {
                        friendsStack.Pop();
                        popCount++;
                    }
                    friendsStack.Push(popularity);
                }
            }
        }

        public static void FinalFriendList(Stack<int> friendsStack)
        {
            foreach (var list in friendsStack.Reverse())
            {
                Console.Write(list);
                Console.Write(" ");
            }
            Console.WriteLine();
        }
    }

    class Friends
    {
        static void Main(string[] args)
        { 
            FriendsMethod.AcceptUserInputs();
        }
    }
}