using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistribution.DBManager
{
    class DbManager: IDbManager
    {
        public SqlConnection getConnection()
        {
            SqlConnection connection = null;
            connection = new SqlConnection(@"data source=ITT-CHAITHRAS\SQLEXPRESS; database=TaskDistribution; integrated security=SSPI");
            return connection;
        }

        public void closeConnection(SqlConnection connect)
        {
            connect.Close();
        }

        public int executeDMLQuery(SqlCommand command)
        {
            return command.ExecuteNonQuery();
            Console.WriteLine("Inserted Successfully");
        }

        public void executeSelectQuery(SqlConnection connection,String query)
        {
            SqlCommand command = new SqlCommand(query, connection);
            command.ExecuteNonQuery();
        }
    }
}
