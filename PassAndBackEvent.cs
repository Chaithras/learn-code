using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootBall
{
    class PassAndBackEvent
    {
        static void Main(string[] args)
        {
            int testCase, passCount;
            int playerId = 0, previousId = 0;
            string passType;
            testCase = Convert.ToInt32(Console.ReadLine());
            while (testCase > 0 && testCase <= 100)
            {
                string passCountAndId = Console.ReadLine();
                string[] tokens = passCountAndId.Split(' ');
                Int32.TryParse(tokens[0], out passCount);
                Int32.TryParse(tokens[1], out playerId);
                while (passCount > 0 && passCount <= 100000 && playerId > 0 && playerId <= 1000000)
                {
                    passType = Console.ReadLine();
                    string[] splitchar = passType.Split(' ');
                    string splitpassType = splitchar[0];
                    if (splitpassType == "B")
                    {
                        int temporaryId = playerId;
                        playerId = previousId;
                        previousId = temporaryId;
                    }
                    else
                    {
                        previousId = playerId;
                        playerId = Convert.ToInt32(splitchar[1]);
                    }
                    passCount--;
                }
                Console.WriteLine("Player {0}", playerId);
                testCase--;
            }
            Console.ReadKey();
        }
    }
}










